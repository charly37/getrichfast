FROM python:3.7.4-buster
LABEL maintainer="charles.walker.37@gmail.com"

RUN python3.7 -m pip install requests iexfinance plotly==4.4.1 requests-cache

COPY ./app /prod

CMD ["bash"]