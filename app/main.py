import sys
import traceback
import os
import glob
import logging

import requests
import requests_cache

from dateutil.parser import parse
from datetime import date, timedelta

# Use for plotly
import pandas
# plotly
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots


aUrl = "https://cloud.iexapis.com/v1/"
aToken = os.environ['TOKEN_IEX_API']
if not aToken:
    sys.exit(1)

aToday = date.today()
# We keep earnings starting today +2 until today +7
aNextWeekDate = aToday + timedelta(days=7)
aTomorrowDate = aToday + timedelta(days=2)
# We check options up to 10 days after earning date
aNbDaysOptionSearch = 20
# Percentage used to consider option - 0.10 means we consider option in the range StockPrice +- 10%
aOptionRange = 0.099
# Max price we agree to pay for option
aMaxOptionPrice = 0.20

class Earning:
    def __init__(self, iEarning):
        self.stock = None
        self.earning = iEarning
        self.earningsAnalyzed = None
        self.actualValue = None
        self.pastEarningsAnalyzed = []

    def getAvgVolume(self) -> int:
        aAvgVol = self.stock["avgTotalVolume"]
        if not aAvgVol:
            # seems sometime it is null (maybe for stock without tradding activity)
            aAvgVol = 0
        return aAvgVol

    def getEarningRepportDate(self) -> date:
        return parse(self.earning["reportDate"]).date()

    def getStockPrice(self) -> float:
        aLatestPrice = self.stock["latestPrice"]
        return aLatestPrice

    def getStockInfo(self):
        url = aUrl + f"stock/{self.earning['symbol']}/quote"

        querystring = {"token": aToken}
        try:
            response = requests.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            logging.error(f"Error when getting stock info details {e}")
            sys.exit(1)

        self.stock = response.json()

    def getPastEarnings(self):
        url = aUrl + f"stock/{self.earning['symbol']}/earnings"

        querystring = {"token": aToken,"last":2}
        try:
            response = requests.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            logging.error(f"Error when getting past earning. info details {e}")
            sys.exit(1)

        #logging.info(f"getPastEarnings response {response.json()}")
        return response.json()

    def analyzePastEarnings(self):
        logging.info(f"checking past earnings for {str(self)}")
        aPastEarnings = self.getPastEarnings()
        for aOnePastEarning in aPastEarnings["earnings"]:
            self.analyzePastEarning(aOnePastEarning)
        #self.pastEarningsAnalyzed = df

    def drawPastEaringsAnalyze(self):
        fig = make_subplots(rows=1, cols=2)
        fig.add_scatter(y=[4, 2, 1], mode="lines", row=1, col=1)
        fig.add_bar(y=[2, 1, 3], row=1, col=2)

    def getPastPriceAtDate(self,date):
        aDateFormatUrl = date.strftime("%Y%m%d")
        url = aUrl + f"stock/{self.earning['symbol']}/chart/date/{aDateFormatUrl}"

        querystring = {"token": aToken,"chartByDay":True}
        try:
            response = requests.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            logging.error(f"Error when getting stock info details {e}")
            sys.exit(1)

        return response.json()

    def getPastPricesAroundDate(self,date,iTimeFrame=10):
        #We get prices for -3 // 10 days after earnings
        aStartingDate = date

        aPastValues = None

        for aDayOffset in range(-3,iTimeFrame):
            aDayToGet = aStartingDate + timedelta(days=aDayOffset)
            #Sat and Sun are empty
            logging.debug(f"checking date {aDayToGet} with weekday {aDayToGet.weekday()}")
            if aDayToGet.weekday()!=5 and aDayToGet.weekday() != 6:
                aPriceInfo = self.getPastPriceAtDate(aDayToGet)
                logging.debug(f"aPriceInfo {aPriceInfo}")
                if len(aPriceInfo) != 0:
                    if aPastValues is None:
                        #need to properly create the dataframe object
                        aPastValues = pandas.DataFrame(aPriceInfo)
                    else:
                        aPastValues=aPastValues.append(aPriceInfo,ignore_index=True)
                        logging.debug(f"Adding value to existing aPastValues: {aPastValues}")
                else:
                    logging.error(f"aPriceInfo was empty for date {aDayToGet}")
        return aPastValues

    def analyzePastEarning(self, iPastEarning):
        logging.info(f"checking past earning {iPastEarning}")

        aLastEarningDate = parse(iPastEarning["EPSReportDate"]).date() 
        aLastEarningReportTime = iPastEarning["announceTime"]
        
        
        aHistPrices = self.getPastPricesAroundDate(aLastEarningDate)
        aPastEarningRapport = {"date":aLastEarningDate,"prices":aHistPrices,"reportTime":aLastEarningReportTime}

        logging.debug(f"aPastEarningRapport: {aPastEarningRapport}")
        self.pastEarningsAnalyzed.append(aPastEarningRapport)

    def getActualPrices(self):
        logging.info(f"Getting Actual value in last 3 months for context")
        a3MonthsPast = aToday - timedelta(days=90)
        aCurrentValue = self.getPastPricesAroundDate(a3MonthsPast,89)
        logging.info(f"aCurrentValue: {aCurrentValue}")
        self.actualValue = aCurrentValue

    def getFilteredOptions(self, iEarningDate) -> list:
        iStock = self.earning["symbol"]
        logging.debug(f"getting option for {iStock} and date {iEarningDate}")
        aPossileOptions = []
        aOptionsDetails = self.__getFilteredOptionsDates()
        logging.debug(f"filtering options {aOptionsDetails}")
        for aOneOptionsDay in aOptionsDetails:
            logging.debug(f"checking {aOneOptionsDay}")
            #date is YYYYMM so to parse it with first day of month we add 01 at the end
            aOptionMonth = parse(aOneOptionsDay+'01').date()
            aEndOptionDate = iEarningDate + timedelta(days=aNbDaysOptionSearch)
            #logging.debug(f"aOptionMonth {aOptionMonth}")
            if (aOptionMonth > aEndOptionDate):
                logging.debug(f"aOneOptionsDay discarded due to date compare to earning {aOneOptionsDay}")
                continue
            
            aOptionDetails = self.__getOptionsDetail(aOneOptionsDay)
            logging.debug(f"aOptionDetails {aOptionDetails}")

            for aOneOption in aOptionDetails:
                if self.doWeKeepThisOption(aOneOption, iEarningDate):
                    aPossileOptions.append(aOneOption)

        logging.debug(f"going to return {len(aPossileOptions)} options")
        return aPossileOptions

    def __getFilteredOptionsDates(self) -> list:
        aOptionsDetails = self.__getOptionsDates()
        aPossileOptionDates = []
        for aOneOptionDate in aOptionsDetails:
            aPossileOptionDates.append(aOneOptionDate)
        return aPossileOptionDates

    def checkCompanyEarning(self):
        logging.info(f"checking {self.earning}")
        aEarningDate = self.getEarningRepportDate()
        aOptionsDetails = self.getFilteredOptions(aEarningDate)
        if len(aOptionsDetails) == 0:
            #logging.error(f"No options after filtering for {self.earning} with option dates {aEarningDate}")
            raise ValueError('No options after filtering')

        logging.debug(f"aOptionsDetails: {aOptionsDetails}")

        aActualStockPrice = self.getStockPrice()

        aDaysAfterEarning = []
        aSides = []
        aStrikePrice = []
        aIndex = []
        aOptionValues = []
        aToGain = []
        aSummaries = []
        aClosingPrices = []

        for aOneoption in aOptionsDetails:
            aNbDays = (parse(aOneoption["expirationDate"]).date() - aEarningDate).days
            # dirty hack to separate the call/put on the graph
            if aOneoption["side"] == "call":
                aNbDays = aNbDays + 0.1
                aToBreakEven = aOneoption["strikePrice"] + aOneoption["aOptionValue"]
            else:
                aNbDays = aNbDays - 0.1
                aToBreakEven = aOneoption["strikePrice"] - aOneoption["aOptionValue"]
            aDaysAfterEarning.append(aNbDays)
            aSides.append(aOneoption["side"])
            aStrikePrice.append(aOneoption["strikePrice"])
            aIndex.append(aOneoption["id"])
            aOptionValues.append(aOneoption["aOptionValue"])
            aClosingPrices.append(aOneoption["closingPrice"])
            aToGain.append(0)
            aDifferenceOptionStockAmount = abs(aOneoption["strikePrice"] - aActualStockPrice)
            aDifferenceOptionStockPercentage = aDifferenceOptionStockAmount/aOneoption["strikePrice"]*100
            aToBreakEvenAmount = abs(aToBreakEven - aActualStockPrice)
            aToBreakEvenPercentage = aToBreakEvenAmount/aActualStockPrice*100

            aOptionSummary = f"strikePrice: {aOneoption['strikePrice']}<br>actualStockPrice: {aActualStockPrice}<br>option Value: {aOneoption['aOptionValue']}<br>option Closing Price: {aOneoption['closingPrice']}<br>difference with stock: {aDifferenceOptionStockAmount:.4f}<br>difference with stock %: {aDifferenceOptionStockPercentage:.4f}<br>difference with stock breakeven: {aToBreakEvenAmount:.4f}<br>difference with stock breakeven %: {aToBreakEvenPercentage:.4f}"
            aSummaries.append(aOptionSummary)

        aData = pandas.DataFrame({'Days': aDaysAfterEarning,
                                'Sides': aSides,
                                'Index': aIndex,
                                'OptionValues': aOptionValues,
                                'ClosingPrices': aClosingPrices,
                                'Gains': aToGain,
                                'Summaries':aSummaries,
                                'Strikes': aStrikePrice})

        logging.debug(f"aData: {aData}")
        self.earningsAnalyzed = aData

        

    def __getOptionsDates(self):
        url = aUrl + f"stock/{self.stock['symbol']}/options"

        querystring = {"token": aToken}

        try:
            response = requests.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions as err:
            logging.error(f"Error when getting option dates {err}")
            raise
        return response.json()

    def __getOptionsDetail(self, iDate):
        url = aUrl + f"stock/{self.stock['symbol']}/options/{iDate}"
        querystring = {"token": aToken}

        try:
            response = requests.request("GET", url, params=querystring)
            response.raise_for_status()
        except requests.exceptions as err:
            logging.error(f"Error when getting option details {err}")
            raise

        return response.json()

    def doWeKeepThisOption(self, iOption, iEarningDate) -> bool:
        iStockValue = self.getStockPrice()
        aNextWeekDate = iEarningDate + timedelta(days=aNbDaysOptionSearch)
        aOptionsDate = parse(iOption["expirationDate"]).date()
        # sometime there is no closingPrice (if no volume) so we enrich the option info with a field which we could use to represent the option value....the average between ask and bid
        # we use the average between ask and bid
        aOptionValue = (iOption["ask"] + iOption["bid"]) / 2
        iOption["aOptionValue"] = aOptionValue

        if (iEarningDate >= aOptionsDate) or (aOptionsDate > aNextWeekDate):
            # we discard the options that expire before the earning or too far in the future
            logging.debug(f"Discarding option due to date compare to earning {iOption}")
            return False
        if (((1 + aOptionRange) * iStockValue < iOption["strikePrice"]) or ((1 - aOptionRange) * iStockValue > iOption["strikePrice"])):
            # We only keep the options with strick price in aOptionRange% range of actual price
            logging.debug(f"Discarding option due to strike value too far from stock price")
            return False
        if (iOption["aOptionValue"] > aMaxOptionPrice):
            # we only keep cheap options
            logging.debug(f"Discarding option due to option value too high")
            return False

        # We pass all test - we are going to keep the option
        return True

    def __repr__(self):
        return f"self.stock:{self.stock['symbol']}__self.earning:{self.earning}"

    def __str__(self):
        return f"stock:{self.earning['symbol']}_earningDate:{self.earning['reportDate']}_stockAvgVol:{self.getAvgVolume()}"


def cleanPreviousFiles():
    # Get a list of all the file paths that ends with .txt from in specified directory
    aOldFiles = glob.glob('*.html')
    # Iterate over the list of filepaths & remove each file.
    for filePath in aOldFiles:
        try:
            os.remove(filePath)
        except:
            logging.error(f"Error while deleting file : {filePath}")

def __getUpcomingEarning():
    url = aUrl + f"stock/market/upcoming-earnings"

    querystring = {"token": aToken}
    try:
        response = requests.request("GET", url, params=querystring)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        logging.error(f"Error when getting upcoming earnings {e}")
        sys.exit(1)

    logging.debug(f"Next week earnings {response.json()}")
    return response.json()

def getNextWeekEarning() -> list:
    aUpcomingEarnings = __getUpcomingEarning()
    aNextWeekEarnings = []
    for aOneEarning in aUpcomingEarnings:
        # We only keep the earnings in the future
        aEarningDate = parse(aOneEarning["reportDate"]).date()
        if aEarningDate < aNextWeekDate and aEarningDate > aTomorrowDate:
            # We get the stock info to create an earning object
            #aStockInfo = __getStockInfo(aOneEarning["symbol"])
            aIncomingEarning = Earning(aOneEarning)
            aIncomingEarning.getStockInfo()
            # We add the earning object
            aNextWeekEarnings.append(aIncomingEarning)

    return aNextWeekEarnings

def drawFigure(iData, iEarning, iPastEarnings, iActualStockValue):
    try:
        logging.info(f"Going to draw figure for {iEarning}")
        fig = make_subplots(rows=2, cols=2,subplot_titles=(f"Past earnings {iPastEarnings[0]['date']}-{iPastEarnings[0]['reportTime']}", f"Past earnings {iPastEarnings[1]['date']}-{iPastEarnings[1]['reportTime']}", "Actual Stock value", "Possible Options"))
        
        #draw earnings
        aRow = 1
        aCol = 1
        for aOnePastEarnings in iPastEarnings:
            logging.debug(f"aOnePastEarnings {aOnePastEarnings} with aRow: {aRow} and aCol: {aCol}")
            fig.add_trace(go.Candlestick(x=aOnePastEarnings['prices']['date'],
                                open=aOnePastEarnings['prices']['open'],
                                high=aOnePastEarnings['prices']['high'],
                                low=aOnePastEarnings['prices']['low'],
                                close=aOnePastEarnings['prices']['close']), row=aRow, col=aCol)
            
            aCol = aCol +1
            if aCol >= 3:
                aCol = 1
                aRow = aRow +1
        

        # Add current stock value in position 3
        logging.info(f"iActualStockValue: {iActualStockValue}")
        fig.add_trace(go.Candlestick(x=iActualStockValue['date'],
                                open=iActualStockValue['open'],
                                high=iActualStockValue['high'],
                                low=iActualStockValue['low'],
                                close=iActualStockValue['close']), row=aRow, col=aCol)
        aCol = aCol +1

        #Remove the share small view for stocks
        fig.update_layout(xaxis_rangeslider_visible=False,xaxis2_rangeslider_visible=False,xaxis3_rangeslider_visible=False)

        aFigTitle = f"{iEarning.earning['symbol']}_{iEarning.earning['reportDate']}"

        aCallStrikesData = iData[(iData['Sides'] == "call")]

        logging.debug(f"going to draw options with aRow: {aRow} and aCol: {aCol}")
        
        if len(aCallStrikesData) != 0:
            aCallStrikesValuesFigures = go.Scatter(x=aCallStrikesData['Days'], 
                                        y=aCallStrikesData['Strikes'], 
                                        mode='markers', 
                                        name="Call Options",
                                        text=aCallStrikesData['Summaries'],
                                        hovertemplate ='Option summary: %{text}', 
                                        marker={'color': '#0026FE', 'sizemode': 'area', 'sizeref': min(aCallStrikesData['OptionValues'])/5.0, 'size': aCallStrikesData['OptionValues']})
            fig.add_trace(aCallStrikesValuesFigures, row=aRow, col=aCol)
        fig.update_layout(title=aFigTitle)
        fig.update_xaxes(title_text="nb days after earnings", row=aRow, col=aCol)

        aPutStrikesData = iData[(iData['Sides'] == "put")]
        if len(aPutStrikesData) != 0:
            
            # aPuttStrikesFigures=go.Scatter(x=aPutStrikesData['Days'], y=aPutStrikesData['Strikes'], name='PutStrikes',mode='markers', text=aPutStrikesData['ClosingPrices'], marker={'color':'#FE0000','sizemode': 'area','sizeref': 0.10875,'size':aPutStrikesData['ClosingPrices']})
            # fig.add_trace(aPuttStrikesFigures)
            aPuttStrikesValuesFigures = go.Scatter(x=aPutStrikesData['Days'], 
                                        y=aPutStrikesData['Strikes'], 
                                        mode='markers', 
                                        name="Put options",
                                        text=aPutStrikesData['Summaries'], 
                                        hovertemplate ='Option summary: %{text}', 
                                        marker={'color': '#FE0000', 'sizemode': 'area', 'sizeref': min(aPutStrikesData['OptionValues'])/5.0, 'size': aPutStrikesData['OptionValues']})
            fig.add_trace(aPuttStrikesValuesFigures, row=aRow, col=aCol)

        logging.debug(f"fig {fig} ")

        aFirstDay = iData['Days'].min()-0.1
        aLatestDay = iData['Days'].max()+0.1
        logging.info(f"aFirstDay {aFirstDay} and aLatestDay {aLatestDay}")

        
        aStockPrice = iEarning.getStockPrice()
        logging.info(f"add stock price {aStockPrice}")
        fig.add_shape(
            go.layout.Shape(
                type="line",
                x0=aFirstDay,
                y0=aStockPrice,
                x1=aLatestDay,
                y1=aStockPrice,
                line=dict(
                    color="#000000",
                    width=3
                )
            ), row=aRow, col=aCol)

        fig.write_html(f"{aFigTitle}.html", auto_open=True)

    except:
        logging.error(f"Error when drawing analyze results {aOneUpcomingEarning} with traceback.format_exc(): {traceback.format_exc()} and error {sys.exc_info()[2]}")
        raise



def __filterPotentialEarning(ioEarningList) -> list:
    aMaxVolumeStockInEarning = sorted(ioEarningList, key=lambda i: i.getAvgVolume())
    # we only keep the half biggest company to maximize the chance to have options - and we put a max limit to 50 to avoid too much data
    aLimit = min(50, len(ioEarningList) // 2) * -1
    logging.info(f"we are limiting the earning wo analyze to : {-1 * aLimit}")
    return aMaxVolumeStockInEarning[aLimit:]


def prettyprintEarningCandidates(iEarningCandidates):
    for aOnePotentialEarning in iEarningCandidates:
        logging.info(f"aOnePotentialEarning: {str(aOnePotentialEarning)}")


logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='myapp.log',
                    filemode='w')

urllib3_log = logging.getLogger("urllib3")
urllib3_log.setLevel(logging.CRITICAL)


logging.info(f"Starting")

cleanPreviousFiles()

requests_cache.install_cache('demo_cache')

aStates={'nbCompanyEarningsUnfilter':0,'nbCompanyEarningsFilter':0,'nbDiscarded':0,'nbAnalyzed':0}

# Just to be safe we dont do too much call....we limit to max 50 company
aPotentialCompanyToCheck = getNextWeekEarning()
logging.info(f"We have {len(aPotentialCompanyToCheck)} potential Earnings before filtering")
aStates['nbCompanyEarningsUnfilter']=len(aPotentialCompanyToCheck)
# prettyprintEarningCandidates(aPotentialCompanyToCheck)

# We keep the biggest companies only to have lot of options (thus data)
aPotentialCompanyToCheck = __filterPotentialEarning(aPotentialCompanyToCheck)
logging.info(f"We have {len(aPotentialCompanyToCheck)} potential Earnings before filtering: ")
aStates['nbCompanyEarningsFilter']=len(aPotentialCompanyToCheck)
prettyprintEarningCandidates(aPotentialCompanyToCheck)

for aOneUpcomingEarning in aPotentialCompanyToCheck:
    try:
        aOneUpcomingEarning.checkCompanyEarning()
        aThisEarningData = aOneUpcomingEarning.earningsAnalyzed
        aOneUpcomingEarning.analyzePastEarnings()
        aPastEarnings = aOneUpcomingEarning.pastEarningsAnalyzed
        aOneUpcomingEarning.getActualPrices()
        aCurrentStockValue = aOneUpcomingEarning.actualValue
        drawFigure(aThisEarningData, aOneUpcomingEarning,aPastEarnings,aCurrentStockValue)
        aStates['nbAnalyzed']=aStates['nbAnalyzed']+1
    except ValueError :
        logging.warning(f"No options availble to analyze {aOneUpcomingEarning}")
        aStates['nbDiscarded']=aStates['nbDiscarded']+1
    except:
        logging.error(f"Error when trying to analyze earnings {aOneUpcomingEarning} with traceback.format_exc(): {traceback.format_exc()} and error {sys.exc_info()[2]}")
        aStates['nbDiscarded']=aStates['nbDiscarded']+1

logging.info(f"aStates: {aStates}")